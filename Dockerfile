FROM ubuntu

ADD app/build/image /opt/yatzy-server

WORKDIR /opt/yatzy-server

CMD ["bin/yatzy-server"]
