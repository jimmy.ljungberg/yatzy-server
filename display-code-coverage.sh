#!/usr/bin/env bash

echo "========== JaCoCo Code Coverage Report =========="
awk -F"," ' \
    { \
      instructions += $4 + $5; covered += $5 \
    } END { \
      print covered, "/", instructions, " instructions covered"; print 100*covered/instructions, "% covered" \
    } \
  ' $1/build/reports/jacoco/test/jacocoTestReport.csv
echo "================================================="
